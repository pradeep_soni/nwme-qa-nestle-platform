define(['jquery'], function ($) {

    $.fn.documents = function(paymentsUrl) {
        this.paymentsUrl                 = paymentsUrl;
        this.paymentsWrap                = $('#payments-wrap');
        this.paymentsLoading             = $('#payments-loading');

        this.getPayments(this.paymentsUrl);
    };

    $.fn.getPayments = function(url) {
        var $this = this;
        $(this.paymentsWrap).html('');
        $(this.paymentsLoading).show();
        $.ajax({
            url: url,
            method: 'get',
            success: function (response) {
                $($this.paymentsLoading).hide();
                $($this.paymentsWrap).html(response);

            }
        });
    };

});
