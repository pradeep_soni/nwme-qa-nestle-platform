define([
    'jquery',
    'uiComponent',
    'Nwuae_GoogleMapAddress/js/google_maps_loader',
    'Magento_Checkout/js/checkout-data',
    'uiRegistry'
], function (
    $,
    Component,
    GoogleMapsLoader,
    checkoutData,
    uiRegistry
) {

    var componentForm = {
        subpremise: 'short_name',
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'long_name',
        country: 'short_name',
        postal_code: 'short_name',
        postal_code_suffix: 'short_name',
        postal_town: 'short_name',
        sublocality_level_1: 'short_name'
    };

    var lookupElement = {
        street_number: 'street_1',
        route: 'street_2',
        locality: 'city',
        administrative_area_level_1: 'region',
        country: 'country_id',
        postal_code: 'postcode'
    };

    var marker = '';
    var map = '';
    var markers = [];

    var googleMapError = false;
    window.gm_authFailure = function () {
        googleMapError = true;
    };

    var googleMapLoaded = true;

    GoogleMapsLoader.done(function () {
        var enabled = window.checkoutConfig.google_map_address.active;

        var geocoder = new google.maps.Geocoder();
        setTimeout(function () {
            if(googleMapLoaded){
            console.log(' call here ');
            if (!googleMapError) {
                if (enabled == '1') {
                    var domID = "location";
                    var location = $("#" + domID);
                    //SHQ18-260
                    var observer = new MutationObserver(function () {
                        observer.disconnect();
                        $("#" + domID).attr("autocomplete", "new-password");
                    });

                    location.each(function () {
                        var element = this;

                        observer.observe(element, {
                            attributes: true,
                            attributeFilter: ['autocomplete']
                        });

                        autocomplete = new google.maps.places.Autocomplete(
                            /** @type {!HTMLInputElement} */(this),
                            {types: ['geocode']}
                        );

                        autocomplete.setComponentRestrictions(
                            {'country': ['qa']}
                        );

                        autocomplete.addListener('place_changed', fillInAddress);

                    });
                }
            }
            var qatar = {lat: 25.286106, lng: 51.534817};
            map = new google.maps.Map(
                document.getElementById('google_map'),
                {zoom: 16, center: qatar}
            );
            marker = new google.maps.Marker({position: qatar, map: map});

            markers.push(marker);

            /*map.addListener('click', function (e) {
                setMapOnAll(null);
                placeMarker(e.latLng, map);
            });*/

            google.maps.event.addListener(map, 'click', function( event ){
                  //alert( "Latitude: only"+event.latLng.lat()+" "+", longitude: "+event.latLng.lng() ); 
                    placeMarker(event.latLng, map);
                });
            googleMapLoaded = false;

            }

        }, 5000);

    }).fail(function () {
        console.error("ERROR: Google maps library failed to load");
    });

    var fillInAddress = function () {
        var place = autocomplete.getPlace();
        var street = [];
        var region  = '';
        var streetNumber = '';
        var city = '';
        var postcode = '';
        var postcodeSuffix = '';
        var lat = '';
        var lng = '';

        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var value = place.address_components[i][componentForm[addressType]];
                if (addressType == 'subpremise') {
                    streetNumber = value + '/';
                } else if (addressType == 'street_number') {
                    streetNumber = streetNumber + value;
                } else if (addressType == 'route') {
                    street[1] = value;
                } else if (addressType == 'administrative_area_level_1') {
                    region = value;
                } else if (addressType == 'sublocality_level_1') {
                    city = value;
                } else if (addressType == 'postal_town') {
                    city = value;
                } else if (addressType == 'locality' && city == '') {
                    //ignore if we are using one of other city values already
                    city = value;
                } else if (addressType == 'postal_code') {
                    postcode = value;
                    var thisDomID = uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.postcode').uid
                    if ($('#'+thisDomID)) {
                        $('#'+thisDomID).val(postcode + postcodeSuffix);
                        $('#'+thisDomID).trigger('change');
                    }
                } else if (addressType == 'postal_code_suffix') {
                    postcodeSuffix = '-' + value;
                    var thisDomID = uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.postcode').uid
                    if ($('#'+thisDomID)) {
                        $('#'+thisDomID).val(postcode + postcodeSuffix);
                        $('#'+thisDomID).trigger('change');
                    }
                } else {
                    var elementId = lookupElement[addressType];
                    var thisDomID = uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.'+ elementId).uid;
                    if ($('#'+thisDomID)) {
                        $('#'+thisDomID).val(value);
                        $('#'+thisDomID).trigger('change');
                    }
                }
            }
        }
        if (street.length > 0) {
            street[0] = streetNumber;
            var domID = uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.street').elems()[0].uid;
            var streetString = street.join(' ');
            if ($('#'+domID)) {
                $('#'+domID).val(streetString);
                $('#'+domID).trigger('change');
            }
        }
        var cityDomID = uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.city').uid;
        if ($('#'+cityDomID)) {
            $('#'+cityDomID).val(city);
            $('#'+cityDomID +' option')
                .filter(function () {
                    return $.trim($(this).text()) == city;
                })
                .attr('selected',true);
            $('#'+cityDomID).trigger('change');
        }
        var latDomID = uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.latitude').uid;
        if ($('#'+latDomID)) {
            var latValue = place.geometry.location.lat().toFixed(4);
            $('#'+latDomID).val(latValue);
            $('#'+latDomID).trigger('change');
        }
        var lngDomID = uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.longitude').uid;
        if ($('#'+lngDomID)) {
            var lngValue = place.geometry.location.lng().toFixed(4);
            $('#'+lngDomID).val(lngValue);
            $('#'+lngDomID).trigger('change');
        }

        createMarker(place.geometry.location.lat(), place.geometry.location.lng());

        var cityDomID = uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.city').uid;
        if ($('#'+cityDomID)) {
            $('#'+cityDomID).val(city);
            $('#'+cityDomID).trigger('change');
        }
        if (region != '') {
            if (uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.region_id')) {
                var regionDomId = uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.region_id').uid;
                if ($('#'+regionDomId)) {
                    //search for and select region using text
                    $('#'+regionDomId +' option')
                        .filter(function () {
                            return $.trim($(this).text()) == region;
                        })
                        .attr('selected',true);
                    $('#'+regionDomId).trigger('change');
                }
            }
            if (uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.region_id_input')) {
                var regionDomId = uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.region_id_input').uid;
                if ($('#'+regionDomId)) {
                    $('#'+regionDomId).val(region);
                    $('#'+regionDomId).trigger('change');
                }
            }
        }
    }

    createMarker = function (lat, lng) {
        // The purpose is to create a single marker, so
        // check if there is already a marker on the map.
        // With a new click on the map the previous
        // marker is removed and a new one is created.

        // If the marker variable contains a value
        if (marker) {
            // remove that marker from the map
            marker.setMap(null);
            // empty marker variable
            marker = "";
        }

        // Set marker variable with new location
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lng),
            draggable: true, // Set draggable option as true
            map: map
        });

        markers.push(marker);


        // This event detects the drag movement of the marker.
        // The event is fired when left button is released.
        google.maps.event.addListener(marker, 'dragend', function () {

            // Updates lat and lng position of the marker.
            marker.position = marker.getPosition();

            // Get lat and lng coordinates.
            var lat = marker.position.lat().toFixed(4);
            var lng = marker.position.lng().toFixed(4);

            // Update lat and lng values into text boxes.
            getCoords(lat, lng);

        });
        map.setZoom(16);
        map.setCenter(new google.maps.LatLng(lat, lng));

        var latDomID = uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.latitude').uid;
        if ($('#'+latDomID)) {
            $('#'+latDomID).val(lat.toFixed(4));
            $('#'+latDomID).trigger('change');
        }
        var lngDomID = uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.longitude').uid;
        if ($('#'+lngDomID)) {
            $('#'+lngDomID).val(lng.toFixed(4));
            $('#'+lngDomID).trigger('change');
        }
    }

    placeMarker = function (position, map) {
        marker = new google.maps.Marker({
            position: position,
            map: map
        });
        
        var latDomID = uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.latitude').uid;
        if ($('#'+latDomID)) {
            var latValue = position.lat().toFixed(4);
            $('#'+latDomID).val(latValue);
            $('#'+latDomID).trigger('change');
        }
        var lngDomID = uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.longitude').uid;
        if ($('#'+lngDomID)) {
            var lngValue = position.lng().toFixed(4);
            $('#'+lngDomID).val(lngValue);
            $('#'+lngDomID).trigger('change');
        }
        map.panTo(position);
        markers.push(marker);
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'latLng': position }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    jQuery('#location').val(results[0].formatted_address);                   

                    fillAddressFields(results[0]);
                   
                }
            }
        });
    }

    fillAddressFields = function (address) {

        var place = address;
        var street = [];
        var region  = '';
        var streetNumber = '';
        var city = '';
        var postcode = '';
        var postcodeSuffix = '';
        var lat = '';
        var lng = '';

        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var value = place.address_components[i][componentForm[addressType]];
                if (addressType == 'subpremise') {
                    streetNumber = value + '/';
                } else if (addressType == 'street_number') {
                    streetNumber = streetNumber + value;
                } else if (addressType == 'route') {
                    street[1] = value;
                } else if (addressType == 'administrative_area_level_1') {
                    region = value;
                } else if (addressType == 'sublocality_level_1') {
                    city = value;
                } else if (addressType == 'postal_town') {
                    city = value;
                } else if (addressType == 'locality' && city == '') {
                    //ignore if we are using one of other city values already
                    city = value;
                } else if (addressType == 'postal_code') {
                    postcode = value;
                    var thisDomID = uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.postcode').uid
                    if ($('#'+thisDomID)) {
                        $('#'+thisDomID).val(postcode + postcodeSuffix);
                        $('#'+thisDomID).trigger('change');
                    }
                } else if (addressType == 'postal_code_suffix') {
                    postcodeSuffix = '-' + value;
                    var thisDomID = uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.postcode').uid
                    if ($('#'+thisDomID)) {
                        $('#'+thisDomID).val(postcode + postcodeSuffix);
                        $('#'+thisDomID).trigger('change');
                    }
                } else {
                    var elementId = lookupElement[addressType];
                    var thisDomID = uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.'+ elementId).uid;
                    if ($('#'+thisDomID)) {
                        $('#'+thisDomID).val(value);
                        $('#'+thisDomID).trigger('change');
                    }
                }
            }
        }
        if (street.length > 0) {
            street[0] = streetNumber;
            var domID = uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.street').elems()[0].uid;
            var streetString = street.join(' ');
            if ($('#'+domID)) {
                $('#'+domID).val(streetString);
                $('#'+domID).trigger('change');
            }
        }
        var cityDomID = uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.city').uid;
        if ($('#'+cityDomID)) {
            $('#'+cityDomID).val(city);
            $('#'+cityDomID +' option')
                .filter(function () {
                    return $.trim($(this).text()) == city;
                })
                .attr('selected',true);
            $('#'+cityDomID).trigger('change');
        }

    }

    setMapOnAll = function (map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    return Component;

});