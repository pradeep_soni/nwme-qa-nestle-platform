define(['jquery'], function ($) {

    $.fn.OrderStatuses = function(getOrderStatusesUrl) {
        this.getOrderStatusesUrl = getOrderStatusesUrl;
        this.orderStatusLoading  = $('#order-status-loading');
        this.orderIncrementIds   = [];

        this.start();
        if (this.orderIncrementIds) {
            this.getOrderStatuses();
        }
    };

    $.fn.start = function() {
        $this = this;
        $('.status').each(function(i, obj) {
            $($this.orderStatusLoading).show();
            $this.orderIncrementIds = $this.orderIncrementIds.concat($(obj).attr('data-id'));
        });
        this.orderIncrementIds = this.orderIncrementIds.join();
    };

    $.fn.getOrderStatuses = function() {
        $this = this;
        $.ajax({
            url: this.getOrderStatusesUrl,
            method: 'get',
            data: {order_ids: $this.orderIncrementIds},
            success: function(response) {
                $($this.orderStatusLoading).hide();
                $.each(response, function(k,v) {
                    if ($('#order-status[data-id="' + k + '"]')) {
                        $('#order-status[data-id="' + k + '"]').html(v);
                    }
                })
            }
        });
    };

});
