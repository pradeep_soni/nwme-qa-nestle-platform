define(['jquery'], function ($) {

    $.fn.documents = function(invoicesUrl, digitalinvoicestatusUrl, paymentsUrl, deliveriesUrl, productDate, productPrice) {
        this.invoicesUrl                 = invoicesUrl;
        this.digitalinvoicestatusUrl     = digitalinvoicestatusUrl;
        this.paymentsUrl                 = paymentsUrl;
        this.deliveriesUrl               = deliveriesUrl;
        this.invoicesWrap                = $('#invoices-wrap');
        this.digitalinvoicestatusWrap    = $('#digitalinvoicestatus-wrap');
        this.paymentsWrap                = $('#payments-wrap');
        this.deliveriesWrap              = $('#deliveries-wrap');
        this.invoicesLoading             = $('#invoices-loading');
        this.digitalinvoicestatusLoading = $('#digitalinvoicestatus-loading');
        this.paymentsLoading             = $('#payments-loading');
        this.deliveriesLoading           = $('#deliveries-loading');

        this.productDate                 = productDate;
        this.productPrice                = productPrice;

        this.getPayments(this.paymentsUrl,this.productDate);
        this.getInvoices(this.invoicesUrl,this.productDate,this.productPrice);
        this.getDigitalInvoiceStatus(this.digitalinvoicestatusUrl);
        this.getDeliveries(this.deliveriesUrl, this.productDate);
    };

    $.fn.getPayments = function(url,date) {
        var $this = this;
        $(this.paymentsWrap).html('');
        $(this.paymentsLoading).show();
        $.ajax({
            url: url,
            data: {product_date: date},
            method: 'get',
            success: function(response){
                $($this.paymentsLoading).hide();
                $($this.paymentsWrap).html(response);

            }
        });
        // new Ajax.Request(
        //     url,
        //     {
        //         method: 'get',
        //         onSuccess: function(response) {
        //             $this.paymentsLoading.hide();
        //             response.responseText = response.responseText.replace('setLocation', 'documents.getPayments');
        //             $this.paymentsWrap.update(response.responseText);
        //             $$('#payments-wrap .pages a').each(function(element) {
        //                 Event.observe(element, 'click', function(e) {
        //                     Event.stop(e);
        //                     $this.getPayments(element.readAttribute('href'));
        //                 });
        //             });
        //         }
        //     }
        // );
    };

    $.fn.getInvoices = function(url,date,price) {
        var $this = this;
        $(this.invoicesWrap).html('');
        $(this.invoicesLoading).show();

        $.ajax({
            url: url,
            data: {product_date: date, product_price: price},
            method: 'get',
            success: function(response){
                $($this.invoicesLoading).hide();
                $($this.invoicesWrap).html(response);

            }
        });
        // new Ajax.Request(
        //     url,
        //     {
        //         method: 'get',
        //         onSuccess: function(response) {
        //             $this.invoicesLoading.hide();
        //             response.responseText = response.responseText.replace('setLocation', 'documents.getInvoices');
        //             $this.invoicesWrap.update(response.responseText);
        //             $$('#invoices-wrap .pages a').each(function(element) {
        //                 Event.observe(element, 'click', function(e) {
        //                     Event.stop(e);
        //                     $this.getInvoices(element.readAttribute('href'));
        //                 });
        //             });
        //         }
        //     }
        // );
    };

    $.fn.getDigitalInvoiceStatus = function(url) {
        var $this = this;
        $(this.digitalinvoicestatusWrap).html('');
        $(this.digitalinvoicestatusLoading).show();
        $.ajax({
            url: url,
            method: 'get',
            success: function(response){
                $($this.digitalinvoicestatusLoading).hide();
                $($this.digitalinvoicestatusWrap).html(response);

            }
        });
        // new Ajax.Request(
        //     $this.digitalinvoicestatusUrl,
        //     {
        //         method: 'get',
        //         onSuccess: function(response) {
        //             $this.digitalinvoicestatusLoading.hide();
        //             $this.digitalinvoicestatusWrap.update(response.responseText);
        //         }
        //     }
        // );

    };

    $.fn.changeDigitalInvoiceStatus = function(url) {
        var $this = this;
        $(this.digitalinvoicestatusWrap).html('');
        $(this.digitalinvoicestatusLoading).show();
        $.ajax({
            url: url,
            method: 'get',
            success: function(response){
                $($this.digitalinvoicestatusLoading).hide();
                $($this.digitalinvoicestatusWrap).html(response);

            }
        });
        // new Ajax.Request(
        //     url,
        //     {
        //         method: 'get',
        //         onSuccess: function(response) {
        //             $this.getDigitalInvoiceStatus();
        //         }
        //     }
        // );
    };

        $.fn.getDeliveries = function(url, date) {
            var $this = this;
            $(this.deliveriesWrap).html('');
            $(this.deliveriesLoading).show();
            $.ajax({
                url: url,
                data: {product_date: date},
                method: 'get',
                success: function(response){
                    $($this.deliveriesLoading).hide();
                    $($this.deliveriesWrap).html(response);

                }
            });
            // new Ajax.Request(
            //     url,
            //     {
            //         method: 'get',
            //         onSuccess: function(response) {
            //             $this.deliveriesLoading.hide();
            //             response.responseText = response.responseText.replace('setLocation', 'documents.getDeliveries');
            //             $this.deliveriesWrap.update(response.responseText);
            //             $$('#deliveries-wrap .pages a').each(function(element) {
            //                 Event.observe(element, 'click', function(e) {
            //                     Event.stop(e);
            //                     $this.getDeliveries(element.readAttribute('href'));
            //                 });
            //             });
            //         }
            //     }
            // );
        };

});
