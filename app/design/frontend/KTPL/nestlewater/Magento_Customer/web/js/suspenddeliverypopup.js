define(['jquery', 'Magento_Ui/js/modal/modal'], function ($, modal) {
    var options = {
        type: 'popup',
        responsive: true,
        innerScroll: true,
        buttons: false
    };

    $.fn.suspendDeliveryPopup = function(controllerUrl, id){
        $.ajax({
            url: controllerUrl,
            data: {id: id},
            method: 'get',
            success: function (response) {
                $('#popup-modal').html(response).modal(options).modal('openModal');
                // $("#events_popup").html('Thanks.');
            }
        });
    }

});
