/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'jquery',
    'ko',
    './customer/address'
], function ($, ko, Address) {
    'use strict';

    var isLoggedIn = ko.observable(window.isCustomerLoggedIn);

    return {
        /**
         * @return {Array}
         */
        getAddressItems: function () {
            var items = [],
                customerData = window.customerData;

            if (isLoggedIn()) {
                if (Object.keys(customerData).length) {
                    $.each(customerData.addresses, function (key, item) {
                        
                        if(typeof customerData.custom_attributes.acw_customer_id === 'undefined'){
                            items.push(new Address(item));  
                        }else{
                            /*if(typeof item.custom_attributes.acw_customer_address_status !== 'undefined' && typeof item.custom_attributes.is_delivery.value !== 'undefined'){ */
                            if(item.custom_attributes.acw_customer_address_status.value == 'active' && item.custom_attributes.is_delivery.value != 0){
                                items.push(new Address(item));  
                            }
                        /*}*/

                        }
                        
                    });
                }
            }

            return items;
        }
    };
});
