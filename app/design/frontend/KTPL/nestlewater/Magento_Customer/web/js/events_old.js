define(['jquery'], function ($) {
    this.calendarUrl = "";
    this.pastText = "";
    this.futureText = "";
    this.eventsTitle = $('.events-title')[0];
    this.eventsKey = $('.events-key')[0];
    this.eventsCalendar = $('.events-calendar')[0];
    this.eventsCalendarLoading = $('#events-calendar-loading');
    this.currentDate = new Date();
    this.currentMonth = this.currentDate.getMonth();
    this.currentMonth = (this.currentMonth + 1);
    this.currentYear = this.currentDate.getFullYear();

    $.fn.EventsCalendar = function(calendarUrl, pastText, futureText) {
        this.calendarUrl = calendarUrl;
        this.pastText = pastText;
        this.futureText = futureText;
        this.eventsTitle = $('.events-title')[0];
        this.eventsKey = $('.events-key')[0];
        this.eventsCalendar = $('.events-calendar')[0];
        this.eventsCalendarLoading = $('#events-calendar-loading');
        this.currentDate = new Date();
        this.currentMonth = this.currentDate.getMonth();
        this.currentMonth = (this.currentMonth + 1);
        this.currentYear = this.currentDate.getFullYear();
        this.getCalendar(this.currentMonth, this.currentYear);
    };

    $.fn.getCalendar = function(month, year) {
        var $this = this;
        $(this.eventsCalendarLoading).show();
        $(this.eventsCalendar).hide();

        $.ajax({
            url: 'http://127.0.0.1/nestlewatersm2/customer/account/events/',
            data: {month: month, year: year},
            success: function(response){
                console.log(response);
                $($this.eventsCalendar).append(response.responseText);
                $($this.eventsCalendarLoading).hide();
                $($this.eventsCalendar).show();
                if (month < $this.currentMonth
                    && year <= $this.currentYear
                ) {
                    $($this.eventsTitle).append($this.pastText);
                    if (!$($this.eventsTitle).hasClassName('past')
                        && !$($this.eventsKey).hasClassName('past')
                        && !$($this.eventsCalendar).hasClassName('past')
                    ) {
                        $($this.eventsTitle).addClassName('past');
                        $($this.eventsKey).addClassName('past');
                        $($this.eventsCalendar).addClassName('past');
                    }
                } else {
                    $($this.eventsTitle).append($this.futureText);
                    if ($($this.eventsTitle).hasClassName('past')
                        && $($this.eventsKey).hasClassName('past')
                        && $($this.eventsCalendar).hasClassName('past')
                    ) {
                        $($this.eventsTitle).removeClassName('past');
                        $($this.eventsKey).removeClassName('past');
                        $($this.eventsCalendar).removeClassName('past');
                    }
                }
            }
        });
    };
});
