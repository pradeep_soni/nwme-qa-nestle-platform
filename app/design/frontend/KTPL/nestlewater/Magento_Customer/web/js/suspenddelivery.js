define(['jquery'], function ($) {
    $.fn.suspendDelivery = function(getSuspendDeliveryUrl){
        $this                          = this;
        this.getSuspendDeliveryUrl     = getSuspendDeliveryUrl;
        this.getSuspendDeliveryLoading = $('#suspend-delivery-loading');
        this.suspendDeliveryIds        = [];
        this.n                         = 0;

        this.start();
        if (this.n > 0) {
            this.getSuspendDelivery();
        }
    };

    $.fn.start = function() {
        $this = this;
        $('#suspend-delivery').each(function(element) {
            $($this.getSuspendDeliveryLoading).show();
            $this.n = $this.n + 1;
        });
    };

    $.fn.getSuspendDelivery = function() {

        $.ajax({
            url: this.getSuspendDeliveryUrl,
            method: 'get',
            success: function(response){
                $($this.getSuspendDeliveryLoading).hide();
                $.each(response, function(k,v) {
                    if ($('#suspend-delivery[data-id="' + k + '"]')) {
                        $('#suspend-delivery[data-id="' + k + '"]').html(v);
                    }
                });
            }
        });
    };

});
