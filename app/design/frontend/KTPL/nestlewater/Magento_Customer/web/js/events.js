define(['jquery'], function ($) {
    this.calendarUrl = "";
    this.pastText = "";
    this.futureText = "";
    this.eventsTitle = $('.events-title')[0];
    this.eventsKey = $('.events-key')[0];
    this.eventsCalendar = $('.events-calendar')[0];
    this.eventsCalendarLoading = $('#events-calendar-loading');
    this.currentDate = new Date();
    this.currentMonth = this.currentDate.getMonth();
    this.currentMonth = (this.currentMonth + 1);
    this.currentYear = this.currentDate.getFullYear();

    $.fn.EventsCalendar = function(calendarUrl, pastText, futureText) {
        this.calendarUrl = calendarUrl;
        this.pastText = pastText;
        this.futureText = futureText;
        this.eventsTitle = $('.events-title')[0];
        this.eventsKey = $('.events-key')[0];
        this.eventsCalendar = $('.events-calendar')[0];
        this.eventsCalendarLoading = $('#events-calendar-loading');
        this.currentDate = new Date();
        this.currentMonth = this.currentDate.getMonth();
        this.currentMonth = (this.currentMonth + 1);
        this.currentYear = this.currentDate.getFullYear();
        this.getCalendar(this.currentMonth, this.currentYear, this.calendarUrl);
    };

    $.fn.getCalendar = function(month, year, calendarUrls) {
        console.log("efds 1");

        // var $this = this;
        pastText = "Past Events";
        futureText = "Upcoming Events";
        $(eventsCalendarLoading).show();
        $(eventsCalendar).hide();

        $.ajax({
            url: calendarUrls,
            method: 'get',
            data: {month: month, year: year},
            success: function(response){
                /*var nResponse = response.split("^^^^^");
                
                if(nResponse[0] != ''){
                    $('.events-calendar').html('');
                }
                $('.events-calendar').append(nResponse[0]);
                if(nResponse[1] != ''){
                    $('.events-calendar-month').html('');
                }
                $('.events-calendar-month').append(nResponse[1]);*/
                $('.events-calendar').html('');
                $('.events-calendar').append(response);
                $(eventsCalendar).append(response.responseText);
                $(eventsCalendarLoading).hide();
                $(eventsCalendar).show();
                if (month < currentMonth
                    && year <= currentYear
                ) {
                    $(eventsTitle).html('');
                    $(eventsTitle).append(pastText);
                    if (!$(eventsTitle).hasClassName('past')
                        && !$(eventsKey).hasClassName('past')
                        && !$(eventsCalendar).hasClassName('past')
                    ) {
                        $(eventsTitle).addClassName('past');
                        $(eventsKey).addClassName('past');
                        $(eventsCalendar).addClassName('past');
                    }
                } else {
                    $(eventsTitle).html('');
                    $(eventsTitle).append(futureText);
                    // if ($(eventsTitle).hasClassName('past')
                    //     && $(eventsKey).hasClassName('past')
                    //     && $(eventsCalendar).hasClassName('past')
                    // ) {
                    //     $(eventsTitle).removeClassName('past');
                    //     $(eventsKey).removeClassName('past');
                    //     $(eventsCalendar).removeClassName('past');
                    // }
                }
            }
        });
    };

    $.fn.getSelectedAcwAddress = function(url, id) {
        console.log(url);
        console.log(id);

        $.ajax({
            url: url,
            method: 'get',
            data: {acw_address_id: id},
            success: function(response) {
                location.reload();
            }
        });
    };
});
