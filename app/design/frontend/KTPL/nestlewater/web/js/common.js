//HEADER STICKY
// require(['jquery'], function($){
//    $(window).scroll(function() {    
//       var scroll = $(window).scrollTop();

//       if(scroll >= 400) {
//           $(".page-header").addClass("fixed-header");
//       } else {
//           $(".page-header").removeClass("fixed-header");
//       }
//   });
// });

//MENU STICKY
require(['jquery'], function($){
   $(window).scroll(function() {    
      var scroll = $(window).scrollTop();

      if(scroll >= 200) {
          $(".nav-sections").addClass("fixed-header");
      } else {
          $(".nav-sections").removeClass("fixed-header");
      }
  });
});

//side menu toggale
require(['jquery'], function($){
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
          panel.style.display = "none";
        } else {
          panel.style.display = "block";
        }
      });
    }
});

//MINCART BTN
require(['jquery'], function($){
    $('input[name$="[qty]"]').focus(function(){
        console.log($(this).attr('id'));
        $('#'+$(this).attr('id')).next('#update-cart-item-331').css('display', 'inline-block');
    });
});