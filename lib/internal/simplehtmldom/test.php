<?php
include('simple_html_dom.php');
$html = '<div data-content-type="row" data-appearance="contained" data-element="main">
		<div data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-element="inner"
			style="justify-content: flex-start; display: flex; flex-direction: column; background-position: left top; background-size: cover; background-repeat: no-repeat; background-attachment: scroll; border-style: none; border-width: 1px; border-radius: 0px; margin: 0px 0px 10px; padding: 10px;">
			<div class="tab-align-left" data-content-type="tabs" data-appearance="default" data-active-tab="0"
				data-element="main" style="margin: 0px; padding: 0px;">
				<div class="tabs-content" data-element="content"
					style="border-width: 0px; border-radius: 0px; min-height: 300px;">
					<div data-content-type="tab-item" data-appearance="default" data-tab-name="Product Information"
						data-background-images="{}" data-element="main" id="BVK1916"
						style="justify-content: flex-start; display: flex; flex-direction: column; background-position: left top; background-size: cover; background-repeat: no-repeat; background-attachment: scroll; border-width: 1px; border-radius: 0px; margin: 0px; padding: 20px 15px;">
						<div data-content-type="html" data-appearance="default" data-element="main"
							style="border-style: solid; border-color: rgb(0, 0, 0); border-width: 0px; border-radius: 0px; margin: 0px; padding: 0px;">
							<div class="accordion-mobile accordion">Product Information</div>
							<div class="accordion-mobile-content panel">
								<div class="responsive-tabel">
									<table border="0" cellpadding="10" cellspacing="0">
										<tbody>
											<tr>
												<th>&nbsp;</th>
												<th>Capacity</th>
												<th>Temperature Range</th>
											</tr>
											<tr>
												<th>Cold Water</th>
												<td>2.7 L</td>
												<td>2-8 C</td>
											</tr>
											<tr>
												<th>Hot Water</th>
												<td>7.0 L</td>
												<td>82-92 C</td>
											</tr>
											<tr>
												<td colspan="3" class="td-divide">&nbsp;</td>
											</tr>
											<tr>
												<th>Width</th>
												<td colspan="2">12.30"</td>
											</tr>
											<tr>
												<th>Depth</th>
												<td colspan="2">12.71”</td>
											</tr>
											<tr>
												<th>Height</th>
												<td colspan="2">38”</td>
											</tr>
											<tr>
												<th>Weight</th>
												<td colspan="2">14.8Kg</td>
											</tr>
											<tr>
												<th>Electrical</th>
												<td colspan="2">220V 240V (+-10%) 50/60Hz<br>
												100V 127V (+-10%) 50/60Hz</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div data-content-type="tab-item" data-appearance="default" data-tab-name="Features and Benefits"
						data-background-images="{}" data-element="main" id="GD0JAY2"
						style="justify-content: flex-start; display: flex; flex-direction: column; background-position: left top; background-size: cover; background-repeat: no-repeat; background-attachment: scroll; border-width: 1px; border-radius: 0px; margin: 0px; padding: 20px 15px;">
						<div data-content-type="html" data-appearance="default" data-element="main"
							style="border-style: none; border-width: 1px; border-radius: 0px; margin: 0px; padding: 0px;">
							<div class="accordion-mobile accordion">Features and Benefits</div>
							<div class="accordion-mobile-content panel">
								<ul>
									<li>Supplied with the power cord</li>
									<li>High gloss cabinet</li>
									<li>ABS plastic</li>
									<li>High polymer polyester coated Steel panel</li>
									<li>Superior resistance to UV light</li>
								</ul>
							</div>
						</div>
					</div>
					<div data-content-type="tab-item" data-appearance="default" data-tab-name="testing"
						data-background-images="{}" data-element="main" id="J8F52H1"
						style="justify-content: flex-start; display: flex; flex-direction: column; background-position: left top; background-size: cover; background-repeat: no-repeat; background-attachment: scroll; border-width: 1px; border-radius: 0px; margin: 0px; padding: 40px;">
						<div data-content-type="html" data-appearance="default" data-element="main"
							style="border-style: none; border-width: 1px; border-radius: 0px; margin: 0px; padding: 0px;">
							<div class="accordion-mobile accordion">testing</div>
							<div class="accordion-mobile-content panel">
								testing testing</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>';

$html = str_get_html($html);
// $html = str_get_html($html);

//$html->find('div', 1)->class = 'bar';
$keys = array();
$values = array();
foreach ($html->find('.accordion-mobile') as $key => $value) 
{
	$code = $value->innertext;
	array_push($keys, $value->innertext);
	// echo $code;
	// echo '<br>';
}
foreach ($html->find('.accordion-mobile-content') as $key => $value) 
{
	$code = $value->innertext;
	array_push($values, $value->innertext);
	// echo $code;
	// echo '<br>';
}

$c = array_combine($keys, $values);

print_r($c);

//print_r($code);
//echo $html; // Output: <div id="hello">foo</div><div id="world" class="bar">W
?>